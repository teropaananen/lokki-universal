const child_process = require('child_process');
const chalk = require('chalk');

const args = process.argv.slice(2);
const platform = 'android';

let bundleOutput = null;
let assetDest = null;

if (platform === 'android') {
  bundleOutput =
    '--bundle-output android/app/src/main/assets/index.android.bundle';
  assetDest = '--assets-dest android/app/src/main/res';
}

const bundleCmd =
  'npx react-native bundle ' +
  [
    '--reset-cache',
    `--platform ${platform}`,
    `--dev false`,
    `--entry-file index.${platform}.js`,
    bundleOutput,
    assetDest,
  ].join(' ');

console.log('Bundling:', bundleCmd);
let result = child_process.execSync(bundleCmd).toString();
console.log(result);

