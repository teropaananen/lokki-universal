import {
  DEFAULT_COTTAGES,
  SAVE_COTTAGES,
  SAVE_BOOKINGS,
  EDIT_BOOKINGS,
  REMOVE_BOOKINGS,
  SET_DATE,
  LOAD_STORAGE_TO_REDUX_STATE,
  SAVE_REDUX_STATE_TO_STORAGE,
  SAVE_SETTINGS,
  SETTINGS_BUSY,
  SETTINGS_ERROR,
  SETTINGS_RESET_ERROR,
  DROPBOX_TO_REDUX,
  CLEAN_DATABASE
} from "./constants";
import  AsyncStorage  from "@react-native-community/async-storage";

// Database --------------------------------------------------------
export function cleanDatabase(date) {
  return {
    type: CLEAN_DATABASE,
    date
  };
}

// Settings --------------------------------------------------------
export function saveSettingsKey(dropboxToken) {
  return {
    type: SAVE_SETTINGS,
    dropboxToken
  };
}
export function settingsBusy(busy) {
  return {
    type: SETTINGS_BUSY,
    busy
  };
}

// Error -----------------------------------------------------------
export function sendError(dispatch, errorMessage) {
  dispatch(settingsBusy(false));
  dispatch(settingsError(errorMessage));
  dispatch(resetError());
}
export function resetError() {
  return {
    type: SETTINGS_RESET_ERROR
  };
}
function settingsError(errorMessage) {
  return {
    type: SETTINGS_ERROR,
    errorMessage
  };
}

// Bookings --------------------------------------------------------
export function saveBookings(date, beginDate, endDate, item, booking) {
  return {
    type: SAVE_BOOKINGS,
    date,
    beginDate,
    endDate,
    item,
    booking
  };
}
export function editBookings(date, beginDateKey, endDateKey, item, booking) {
  return {
    type: EDIT_BOOKINGS,
    date,
    beginDateKey,
    endDateKey,
    item,
    booking
  };
}
export function removeBookings(date, item, userId, beginDateKey, endDateKey) {
  return {
    type: REMOVE_BOOKINGS,
    date,
    item,
    userId,
    beginDateKey,
    endDateKey
  };
}

// Redux to Storage ------------------------------------------------
export function loadStateFromStorage() {
  return dispatch => {
    AsyncStorage.getItem("completeStore")
      .then(value => {
        if (value && value.length) {
          try {
            console.log("Will load state from Store");
            const storageData = JSON.parse(value);
            dispatch(saveStorageToReduxState(storageData));
          } catch (error) {
            console.log("Saving to Storage failed:" + error.message);
          }
        } else {
          dispatch(saveCottages(DEFAULT_COTTAGES));
        }
      })
      .catch(error => {
        console.log("Loading from Storage failed:" + error.message);
      });
  };
}
function saveStorageToReduxState(data) {
  return {
    type: LOAD_STORAGE_TO_REDUX_STATE,
    data
  };
}
export function saveReduxStateToStorage() {
  return (dispatch, getState) => {
    try {
      console.log("Will save state to Store");
      let reduxState = JSON.stringify(getState());
      AsyncStorage.setItem("completeStore", reduxState);
    } catch (error) {
      console.log("Saving redux to storage failed", error.message);
    }
  };
}

// Dropbox ---------------------------------------------------------
function prepareState(state) {
  let newState = { ...state };
  if (newState.settings && newState.settings.busy) {
    newState.settings.busy = false;
  }
  if (newState.errorMessage && newState.errorMessage.length > 0) {
    newState.errorMessage = "";
  }
  return newState;
}
function saveDropboxToReduxState(data) {
  return {
    type: DROPBOX_TO_REDUX,
    data
  };
}


// Other -----------------------------------------------------------
export function saveCottages(data) {
  return {
    type: SAVE_COTTAGES,
    data
  };
}
export function setDate(date) {
  return {
    type: SET_DATE,
    date
  };
}
