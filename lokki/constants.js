
export const SAVE_COTTAGES = 'SAVE_COTTAGES';

export const SAVE_BOOKINGS = 'SAVE_BOOKINGS';
export const EDIT_BOOKINGS = 'EDIT_BOOKINGS';

export const REMOVE_BOOKINGS = 'REMOVE_BOOKINGS';

export const SAVE_SETTINGS = 'SAVE_SETTINGS';
export const SETTINGS_BUSY = 'SETTINGS_BUSY';
export const SETTINGS_ERROR = 'SETTINGS_ERROR';
export const SETTINGS_RESET_ERROR = 'SETTINGS_RESET_ERROR';

export const CLEAN_DATABASE = 'CLEAN_DATABASE';

export const SET_DATE = 'SET_DATE';

export const BOOKING_ITEM = {name:'', phone:'', other:''};

export const DEFAULT_COTTAGES = [
    {key:'1', name:'1', img:'mokki1.png'},
    {key:'2', name:'2', img:'mokki2.png'},
    {key:'3', name:'3', img:'mokki3.png'},
    {key:'4', name:'4', img:'mokki4.png'},
    {key:'5', name:'5', img:'mokki5.png'},
    {key:'6', name:'6', img:'mokki6.png'},
    {key:'7', name:'7', img:'mokki7.png'},
    {key:'8', name:'8', img:'mokki8.png'},
    {key:'9', name:'Sauna', img:'rantasauna.png'},
    {key:'10', name:'9', img:'mokki9.png'}
  ];

export const LOAD_STORAGE_TO_REDUX_STATE = 'LOAD_STORAGE_TO_REDUX_STATE';
export const SAVE_REDUX_STATE_TO_STORAGE = 'SAVE_REDUX_STATE_TO_STORAGE';

export const DROPBOX_TO_REDUX = 'DROPBOX_TO_REDUX';

export const TINT_COLOR = "rgba(38,106,46,1.0)";
export const TINT_COLOR_LIGHT = "rgba(38,106,46,0.5)";

export const MAX_DATES_TO_BOOK = 14; // 2 weeks
