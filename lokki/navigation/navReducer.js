import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';
import { AppStackNavigator } from './appNavigator';

const initialStackState = AppStackNavigator.router.getStateForAction(AppStackNavigator.router.getActionForPathAndParams('Home'));
const navStackReducer = (state = initialStackState, action) => {
  const nextState = AppStackNavigator.router.getStateForAction(action, state);
  let ret = nextState || state;
  return ret;
};


const AppReducer = combineReducers({
  nav: navStackReducer,
});

export default AppReducer;