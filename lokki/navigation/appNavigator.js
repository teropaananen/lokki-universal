import React from "react";
import { connect } from "react-redux";
import { Platform } from "react-native";

import { createStackNavigator } from 'react-navigation';

import { TINT_COLOR } from "../constants";

import HomeScreen from "../components/home";
import BookScreen from "../components/book";

// https://reactnavigation.org/docs/3.x/stack-navigator


export const AppStackNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  Book: { screen: BookScreen }
});
