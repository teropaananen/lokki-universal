import React from "react";
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  Text,
  ScrollView,
  Dimensions,
  Alert,
  KeyboardAvoidingView,
  Platform,
  Keyboard
} from "react-native";
import BackgroundView from "./backgroundView";
import EndDateView from "./endDateView";
import IconView from "./iconView";
import { connect } from "react-redux";
import { BOOKING_ITEM } from "../constants";
import {
  saveBookings,
  editBookings,
  removeBookings,
  saveReduxStateToStorage
} from "../actions";
import {
  getDateKey,
  getNextFreeDates,
  getDateKeys,
  getBooking,
  getNextDayFromDate,
  isSameDate,
  getUserId,
  getDateFromKey
} from "../helpers";
import { TINT_COLOR, TINT_COLOR_LIGHT } from "../constants";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

var dateFormat = require("dateformat");

class BookView extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Book " + navigation.state.params.item.name,
    tabBarLabel: "Cottages",
    headerTintColor: Platform.OS === "android" ? "black" : TINT_COLOR,
    tabBarIcon: ({ tintColor }) => (
      <FontAwesomeIcon
        name={"list"}
        style={{
          fontSize: 30,
          color: tintColor
        }}
      />
    )
  });

  constructor(props) {
    super(props);

    const { width, height } = Dimensions.get("window");

    const isEditing = this.props.booking.userId ? true : false;
    let endDate = getNextDayFromDate(this.props.navigation.state.params.date);
    if (isEditing) {
      endDate = getDateFromKey(this.props.booking.endDateKey);
    }

    const name = this.props.booking.name || "";
    const userId = this.props.booking.userId || getUserId();

    this.state = {
      width,
      height,
      dirty: false,
      name,
      userId,
      phone: this.props.booking.phone || "",
      other: this.props.booking.other || "",
      endDate,
      isEditing
    };
    this.onDateSelected = this.onDateSelected.bind(this);
    this._remove = this._remove.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps.booking });
  }

  componentWillUnmount() {
    this._saveStateToRedux();
  }

  _getDateString(date) {
    // https://www.npmjs.com/package/dateformat
    return (
      "(W" + dateFormat(date, "W") + ") " + dateFormat(date, "ddd dd.mm.yyyy")
    );
  }

  render() {
    const { item, date } = this.props.navigation.state.params;
    const containerStyle = [styles.container, this._getMargings()];

    const iconStyle = {
      marginTop: 20,
      fontSize: 40,
      width: 50,
      height: 50,
      alignSelf: "flex-end",
      color: "red",
      textAlign: "center"
    };

    return (
      <View style={{flex:1, backgroundColor:"black"}}>
        <KeyboardAvoidingView behavior="height" style={containerStyle}>
          <ScrollView style={{flex:1}}>
            <Text style={styles.date}>{this._getDateString(date)}</Text>
            <TextInput
              style={styles.input}
              value={this.state.name}
              onChangeText={text => this.setState({ name: text, dirty: true })}
              autoCorrect={false}
              placeholderTextColor={"gray"}
              underlineColorAndroid={"transparent"}
              placeholder={"Customer"}
            />
            <TextInput
              style={styles.input}
              value={this.state.phone}
              onChangeText={text => this.setState({ phone: text, dirty: true })}
              autoCorrect={false}
              keyboardType={"numeric"}
              placeholderTextColor={"gray"}
              underlineColorAndroid={"transparent"}
              placeholder={"Phone"}
            />
            <TextInput
              style={styles.input}
              value={this.state.other}
              onChangeText={text => this.setState({ other: text, dirty: true })}
              autoCorrect={false}
              placeholderTextColor={"gray"}
              underlineColorAndroid={"transparent"}
              placeholder={"Other"}
            />
            {!this.state.isEditing && this.props.freeDates && (
              <EndDateView
                dates={this.props.freeDates}
                style={styles.endDate}
                onDateSelected={this.onDateSelected}
              />
            )}
            {this.state.isEditing && (
              <IconView
                style={iconStyle}
                icon={"trash"}
                onPress={this._remove}
              />
            )}
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }

  onDateSelected(date) {
    this.setState({ endDate: date, dirty: true });
  }

  _getHeight() {
    return {
      height: this.state.height,
      width: this.state.width
    };
  }

  _getMargings() {
    return {
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      marginBottom: 10
    };
  }

  _remove() {
    Alert.alert("Remove", "Remove Booking?\n\n" + this.state.name, [
      { text: "YES", onPress: () => this._doRemove() },
      {
        text: "NO",
        onPress: () => this.props.navigation.goBack(),
        style: "cancel"
      }
    ]);
  }

  _doRemove() {
    if (this.props.booking) {
      const { userId, endDate } = this.state;
      this.props.removeBooking(
        userId,
        this.props.booking.beginDateKey,
        getDateKey(endDate)
      );
    }
    this.props.navigation.goBack();
  }

  _saveStateToRedux() {
    const {
      isEditing,
      userId,
      name,
      phone,
      other,
      dirty,
      endDate
    } = this.state;
    if (isEditing) {
      // Edit booking
      if (dirty) {
        this.props.saveChanges(
          userId,
          name,
          phone,
          other,
          this.props.booking.beginDateKey,
          getDateKey(endDate)
        );
      }
    } else {
      // New booking
      if (dirty && name.length > 0) {
        this.props.saveNewBooking(userId, name, phone, other, endDate);
      }
    }

    Keyboard.dismiss();
  }
}

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  const { date, item } = ownProps.navigation.state.params;
  return {
    booking: getBooking(state.booking, date, item) || BOOKING_ITEM,
    freeDates: getNextFreeDates(state.booking, date, item),
    item
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveNewBooking: (userId, name, phone, other, endDate) => {
      let { date, item } = ownProps.navigation.state.params;
      const beginDate = new Date(date);

      if (isSameDate(beginDate, endDate)) {
        endDate = getNextDayFromDate(endDate);
      }

      save(dispatch, {
        date,
        beginDate,
        endDate,
        item,
        userId,
        name,
        phone,
        other
      });
      dispatch(saveReduxStateToStorage());
    },
    saveChanges: (userId, name, phone, other, beginDateKey, endDateKey) => {
      let { date, item } = ownProps.navigation.state.params;
      dispatch(
        editBookings(date, beginDateKey, endDateKey, item, {
          name,
          phone,
          other,
          userId
        })
      );
      dispatch(saveReduxStateToStorage());
    },
    removeBooking: (userId, beginDateKey, endDateKey) => {
      let { date, item } = ownProps.navigation.state.params;
      dispatch(removeBookings(date, item, userId, beginDateKey, endDateKey));
      dispatch(saveReduxStateToStorage());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookView);

/* -----------------------------------------------------------------
  Helper
*/
function save(dispatch, data) {
  const { date, beginDate, endDate, item, userId, name, phone, other } = data;

  if (date && endDate && item) {
    let indexDate = new Date(date);
    while (!isSameDate(indexDate, endDate)) {
      dispatch(
        saveBookings(indexDate, beginDate, endDate, item, {
          userId,
          name,
          phone,
          other
        })
      );
      indexDate = getNextDayFromDate(indexDate);
    }
  }
}

/* -----------------------------------------------------------------
  StyleSheet
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(230, 230, 230, 1.0)",
    padding: 20,
    borderWidth: 1.0,
    borderColor: "white",
    borderRadius: 4
  },
  date: {
    backgroundColor: TINT_COLOR,
    textAlign: "center",
    color: "white",
    fontSize: 20,
    height: 40,
    padding: 5
  },
  endDate: {
    marginTop: 16
  },
  input: {
    marginTop: 16,
    height: Platform.OS === "android" ? 44 : 44,
    fontSize: 22,
    backgroundColor: "white"
  }
});
