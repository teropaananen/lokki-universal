import React from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  Platform,
  Keyboard
} from "react-native";
import { connect } from "react-redux";
import { saveSettingsKey, cleanDatabase } from "../actions";
import { TINT_COLOR } from "../constants";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { store } from "../app";

class Settings extends React.Component {
  static navigationOptions = {
    title: "Settings",
    headerTintColor: Platform.OS === "android" ? "black" : TINT_COLOR,
    tabBarLabel: "Settings",
    tabBarIcon: ({ tintColor }) => (
      <FontAwesomeIcon
        name={"cog"}
        style={{
          fontSize: 30,
          color: tintColor
        }}
      />
    )
  };

  constructor(props) {
    super(props);

    this.state = {
      dirty: false,
      dropboxToken: this.props.dropboxToken
    };

    this._cancel = this._cancel.bind(this);
    this._saveStateToRedux = this._saveStateToRedux.bind(this);
    this._saveToDropbox = this._saveToDropbox.bind(this);
    this._loadFromDropbox = this._loadFromDropbox.bind(this);
    this._cleanDatabase = this._cleanDatabase.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage && nextProps.errorMessage.length > 0) {
      this._showError(nextProps.errorMessage);
    }
  }


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Dropbox</Text>
        <TextInput
          style={styles.dropboxInput}
          value={this.state.dropboxToken}
          onChangeText={text =>
            this.setState({ dropboxToken: text, dirty: true })}
          autoCorrect={false}
          placeholderTextColor={"gray"}
          placeholder={"Insert token for Dropbox"}
        />
        <TouchableHighlight
          style={[styles.button, { marginTop: 10 }]}
          onPress={this._saveStateToRedux}
        >
          <Text style={styles.buttonText}>Save token</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[styles.buttonMostUsed, { marginTop: 50 }]}
          onPress={this._saveToDropbox}
        >
          <Text style={styles.buttonText}>Save to Dropbox</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[styles.button, { marginTop: 50 }]}
          onPress={this._loadFromDropbox}
        >
          <Text style={styles.buttonText}>Load from Dropbox</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[styles.button, { marginTop: 50 }]}
          onPress={this._cleanDatabase}
        >
          <Text style={styles.buttonText}>Clean database</Text>
        </TouchableHighlight>

        {Boolean(this.props.busy) && (
          <View style={styles.indicatorWrapper}>
            <ActivityIndicator animating={true} size="large" color="white" />
          </View>
        )}
      </View>
    );
  }

  _saveStateToRedux() {
    this._closeKeyboard(0);

    Alert.alert("Settings", "Save new token?", [
      { text: "YES", onPress: () => this._doSaveStateToRedux() },
      {
        text: "NO",
        onPress: () => this._cancel(),
        style: "cancel"
      }
    ]);
  }

  _doSaveStateToRedux() {
    if (this.state.dirty) {
      this.props.saveSettingsKey(this.state.dropboxToken);
    }
    this._closeKeyboard(1000);
  }

  _closeKeyboard(after) {
    setTimeout(()=>{
      Keyboard.dismiss();
    }),after;
  }

  _cancel() {
    this.setState({ dirty: false, dropboxToken: this.props.dropboxToken });
    //this.props.navigation.goBack();
    this._closeKeyboard(1000);
  }

  _saveToDropbox() {
    Alert.alert("Settings", "Save bookings to Dropbox?", [
      { text: "YES", onPress: () => this._doSaveToDropbox() },
      {
        text: "NO",
        onPress: () => this._cancel(),
        style: "cancel"
      }
    ]);
  }

  _doSaveToDropbox() {
    this.props.saveToDropbox(this.state.dropboxToken, store);
  }

  _loadFromDropbox() {
    Alert.alert("Settings", "Load bookings from Dropbox?", [
      { text: "YES", onPress: () => this._doLoadFromDropbox() },
      {
        text: "NO",
        onPress: () => this._cancel(),
        style: "cancel"
      }
    ]);
  }

  _doLoadFromDropbox() {
    this.props.downloadStateFromDropbox(this.state.dropboxToken);
  }

  _cleanDatabase() {
    Alert.alert("Settings", "Remove old bookings from database?", [
      { text: "YES", onPress: () => this._doCleanDatabase() },
      {
        text: "NO",
        onPress: () => this._cancel(),
        style: "cancel"
      }
    ]);
  }

  _doCleanDatabase() {
    this.props.cleanDatabase(new Date());
  }

  _showError(message) {
    console.log("ERROR",message);
    Alert.alert("Error", message, [
      {
        text: "CLOSE"
      }
    ]);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 50
  },
  text: {
    color: "black",
    fontSize: 14,
    paddingVertical: 6
  },
  dropboxInput: {
    height: Platform.OS === "android" ?  40 : 40,
    fontSize: 18,
    color: "white",
    backgroundColor: "black"
  },
  button: {
    height: 40,
    backgroundColor: "gray",
    padding: 6
  },
  buttonMostUsed: {
    height: 60,
    backgroundColor: TINT_COLOR,
    padding: 14
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    textAlign: "center"
  },
  indicatorWrapper: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  return {
    dropboxToken: state.settings.dropboxToken,
    busy: state.settings.busy,
    errorMessage: state.settings.errorMessage
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveSettingsKey: dropboxToken => {
      dispatch(saveSettingsKey(dropboxToken));
    },
    downloadStateFromDropbox: dropboxToken => {
      //dispatch(loadStateFromDropbox(dropboxToken));
    },
    saveToDropbox: (dropboxToken, store) => {
      let state = store.getState();
      //dispatch(saveStateToDropbox(dropboxToken, state));
    },
    cleanDatabase: date => {
      dispatch(cleanDatabase(date));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
