import React from "react";
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import { connect } from "react-redux";
import BackgroundView from "./backgroundView";
import { dateToString, dateStringToDate } from "../reducers/date";
import { getBooking, getDateKey } from "../helpers";
import _ from 'lodash';

class CottageListItem extends React.Component {
  debouncePress;

  constructor(props) {
    super(props);

    this._onPress = this._onPress.bind(this);

    this.debouncePress = _.debounce(
      () => {
        this.props.onPress({ item: this.props.item, id: this.props.id });
      },
      500,
      { leading: true, trailing: false }
    );
  }

  render() {
    return (
      <BackgroundView source={{ static: true, uri: this.props.item.img }}>
        <TouchableHighlight
          onPress={this._onPress}
          underlayColor={"rgba(206, 228, 217, 0.4)"}
        >
          <View style={styles.container}>
            <Text style={styles.text}>{this.props.item.name}</Text>
            {this.props.booking && (
              <Text style={styles.booking}>{this.props.booking.name}</Text>
            )}
          </View>
        </TouchableHighlight>
      </BackgroundView>
    );
  }

  componentWillUnmount() {
    this.debouncePress.cancel();
  }

  _onPress() {
    this.debouncePress();
  }
}

const styles = StyleSheet.create({
  container: {
    height: 140,
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    borderBottomWidth: 1.0,
    borderBottomColor: "white"
  },
  text: {
    left: 20,
    fontSize: 100,
    color: "white",
    textShadowColor: "black",
    textShadowOffset: { width: 1, height: 1 }
  },
  booking: {
    color: "white",
    fontSize: 30,
    left: 40,
    right: 10,
    textShadowColor: "black",
    textShadowOffset: { width: 1, height: 1 }
  }
});

/* -----------------------------------------------------------------
  mapStateToProps
*/
function mapStateToProps(state, ownProps) {
  const date = dateStringToDate(state.date.activeDate);
  return {
    date,
    booking: getBooking(state.booking, date, ownProps.item)
  };
}

/* -----------------------------------------------------------------
  mapDispatchToProps
*/
function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(CottageListItem);
