import React from "react";
import { View, StyleSheet, TouchableHighlight } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

export default class IconView extends React.Component {
  render() {
    const { fontSize, textAlign, width, height, color, ...rest}  = this.props.style;
    return (
      <TouchableHighlight style={width, height, rest} onPress={this.props.onPress}>
        <FontAwesomeIcon
          name={this.props.icon}
          style={[styles.icon, {fontSize, textAlign, color, width, height}]}
        />
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    textShadowColor: "rgba(0,0,0,0.2)",
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
    padding: 6,
    backgroundColor: "transparent"
  }
});
