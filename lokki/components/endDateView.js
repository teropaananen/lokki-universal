import React from "react";
import { FlatList, Text, TouchableHighlight, StyleSheet } from "react-native";
import { TINT_COLOR } from "../constants";

var dateFormat = require("dateformat");

class EndDateView extends React.Component {
  constructor(props) {
    super(props);

    this.state = { selectedId: 0 };

    this._renderItem = this._renderItem.bind(this);
    this._getData = this._getData.bind(this);
    this._onPress = this._onPress.bind(this);
  }

  render() {
    return (
      <FlatList
        horizontal={true}
        style={styles.list}
        data={this._getData()}
        renderItem={this._renderItem}
        keyExtractor={item => item.id}
      />
    );
  }

  _getData() {
    return this.props.dates.map((date, index) => {
      return { id: index, date: this._getDateString(date) };
    });
  }

  _renderItem({ item }) {
    const props = { ...item, ...{ selectedId: this.state.selectedId } };
    return (
      <ListItem
        {...props}
        style={[this.props.style, styles.item]}
        onClick={this._onPress}
      />
    );
  }

  _getDateString(date) {
    // https://www.npmjs.com/package/dateformat
    return dateFormat(date, "ddd dd.mm");
  }

  _onPress(id) {
    this.setState({ selectedId: id });
    if (this.props.onDateSelected) {
      const date = this.props.dates[id];
      this.props.onDateSelected(date);
    }
  }
}

const ListItem = ({ id, date, style, onClick, selectedId }) => {
  let selectionStyle = {};
  if (id <= selectedId) {
    selectionStyle = { backgroundColor: TINT_COLOR };
  }

  return (
    <TouchableHighlight
      style={[style, selectionStyle]}
      onPress={() => onClick(id)}
    >
      <Text style={styles.date}>{date}</Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1
  },
  item: {
    height: 60,
    width: 120,
    justifyContent: "center",
    alignItems: "center",
    borderRightWidth: 2.0,
    borderRightColor: "white",
    backgroundColor: "black"
  },
  date: {
    color: "white",
    fontSize: 18
  }
});

export default EndDateView;
