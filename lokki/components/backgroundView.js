import React from "react";
import { Image, ImageBackground, Platform } from "react-native";

// http://facebook.github.io/react-native/releases/0.48/docs/images.html#background-image-via-nesting

export default class BackgroundView extends React.Component {
  render() {
    let source = null;

    if (this.props.source && this.props.source.uri) {
      // Source
      if (Platform.OS === "android") {
        // Android
        source = { static: true, uri: this.props.source.uri.split(".")[0] };
      } else {
        // iOS
        source = this.props.source;
      }
    } else if (this.props.require) {
      // Require
      source = this.props.require;
    }

    return (
      <ImageBackground
        ref={component => (this._root = component)}
        source={source}
        style={{ flex: 1 }}
      >
        {this.props.children}
      </ImageBackground>
    );
  }
}
