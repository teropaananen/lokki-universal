const { execSync } = require('child_process');


try {
  process.stdout.write('Cleaning...\n');
  execSync('./gradlew clean', { cwd: './android', stdio: 'ignore' });
  execSync('./gradlew cleanBuildCache', { cwd: './android' , stdio: 'ignore'});
  process.stdout.write('Cleaning. Done.\n');

  process.stdout.write('JS bundle build...\n');
  execSync('yarn bundle');
  process.stdout.write('JS bundle build. Done.\n');

  process.stdout.write('Release build...\n');
  execSync('./gradlew app:assembleRelease -x bundleReleaseJsAndAssets --debug', { cwd: './android', stdio: 'ignore'});
  process.stdout.write('Release build. Done.\n');

  process.stdout.write('Install build...\n');
  execSync('./gradlew app:installRelease -x bundleReleaseJsAndAssets --debug', { cwd: './android', stdio: 'ignore'});
  process.stdout.write('Install build. Done.\n');
      
} catch (error) {
  process.stdout.write('Error',error.message);
}
