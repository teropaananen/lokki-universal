import { SAVE_COTTAGES } from "../constants";

const initialState = {
  list: []
};

export default function cottageReducer(state = initialState, action) {
  switch (action.type) {
    case SAVE_COTTAGES: {
      if (state.list && state.list.length > 0) {
        return state;
      } else {
        console.log("Set default cottages", state)
        return {
          ...state,
          list: action.data
        };
      }
    }
    default:
      return state;
  }
}
