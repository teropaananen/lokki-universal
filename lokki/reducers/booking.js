import {
  SAVE_BOOKINGS,
  EDIT_BOOKINGS,
  REMOVE_BOOKINGS,
  MAX_DATES_TO_BOOK,
  CLEAN_DATABASE
} from "../constants";
import {
  getDateKey,
  getDateKeys,
  getDateFromKey,
  getNewDateFromDate,
  getDateKeysFromDates
} from "../helpers";

// ReadMe
// http://www.benmvp.com/learning-es6-enhanced-object-literals/
// https://github.com/lodash/lodash/wiki/FP-Guide

/*
 reservationsByDay: { 
   "dateKey": { 
     bookings: { 
       '1': { 
         name: 'Aaaa', phone: '', other: '' 
        } 
      } 
    } 
  }  
*/

const initialState = {
  reservationsByDay: {}
};

export default function bookingReducer(state = initialState, action) {
  switch (action.type) {
    case SAVE_BOOKINGS: {
      return saveBookings(state, action);
    }
    case REMOVE_BOOKINGS: {
      return removeBookings(state, action);
    }
    case EDIT_BOOKINGS: {
      return editBookings(state, action);
    }
    case CLEAN_DATABASE: {
      const newState = cleanDatabase(state, action);
      return {
        ...newState
      };
    }
    default:
      return state;
  }
}

function saveBookings(state, action) {
  const dateKey = getDateKey(action.date);
  const beginDateKey = getDateKey(action.beginDate);
  const endDateKey = getDateKey(action.endDate);
  const cottageKey = action.item.key;
  const { name, phone, other, userId } = action.booking;

  if (state.reservationsByDay[dateKey]) {
    // update cottage bookings for same date
    const { bookings } = state.reservationsByDay[dateKey];
    return {
      reservationsByDay: {
        ...state.reservationsByDay,
        [dateKey]: {
          ...state.reservationsByDay[dateKey],
          bookings: {
            ...bookings,
            [cottageKey]: {
              userId,
              name,
              phone,
              other,
              beginDateKey,
              endDateKey
            }
          }
        }
      }
    };
  } else {
    // insert new cottage booking for date
    return {
      reservationsByDay: {
        ...state.reservationsByDay,
        ...{
          [dateKey]: {
            bookings: {
              [cottageKey]: {
                userId,
                name,
                phone,
                other,
                beginDateKey,
                endDateKey
              }
            }
          }
        }
      }
    };
  }
}

function removeBookings(state, action) {
  const beginDate = getDateFromKey(action.beginDateKey);
  const endDate = getDateFromKey(action.endDateKey);
  let dateKeys = getDateKeysFromDates(beginDate, endDate);

  let newState = { ...state };

  while (dateKeys.length > 0) {
    const dateKey = dateKeys.pop();
    let reservationsInState = newState.reservationsByDay[dateKey] || {};
    if (
      reservationsInState.bookings &&
      reservationsInState.bookings[action.item.key] &&
      reservationsInState.bookings[action.item.key].userId === action.userId
    ) {
      delete reservationsInState.bookings[action.item.key];
      newState.reservationsByDay[dateKey] = reservationsInState;
    }
  }

  return { ...newState };
}

function editBookings(state, action) {
  const cottageKey = action.item.key;
  const beginDate = getDateFromKey(action.beginDateKey);
  const endDate = getDateFromKey(action.endDateKey);
  let dateKeys = getDateKeysFromDates(beginDate, endDate);

  let newState = { ...state };

  while (dateKeys.length > 0) {
    const dateKey = dateKeys.pop();
    let reservationsInState = newState.reservationsByDay[dateKey] || {};
    if (
      reservationsInState.bookings &&
      reservationsInState.bookings[action.item.key] &&
      reservationsInState.bookings[action.item.key].userId ===
        action.booking.userId
    ) {
      reservationsInState.bookings[action.item.key].name = action.booking.name;
      reservationsInState.bookings[action.item.key].phone =
        action.booking.phone;
      reservationsInState.bookings[action.item.key].other =
        action.booking.other;
      newState.reservationsByDay[dateKey] = reservationsInState;
    }
  }

  return { ...newState };
}

function cleanDatabase(state, action) {
  const today = action.date;

  let newState = { ...state };

  // Get previous month
  let year = today.getFullYear();
  let month = today.getMonth();
  let day = 1; // first day
  if (month === 11) {
    // 0=january
    month = 0;
    year--;
  } else {
    month--;
  }
  const date = new Date(year, month, day);
  const keys = Object.keys(newState.reservationsByDay);
  keys.forEach(key => {
    const dateFromKey = getDateFromKey(key);
    if (dateFromKey < date) {
      delete newState.reservationsByDay[key];
    }
  });

  return newState;
}
