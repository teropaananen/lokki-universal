import {
  SAVE_SETTINGS,
  SETTINGS_BUSY,
  SETTINGS_ERROR,
  SETTINGS_RESET_ERROR
} from "../constants";

const initialState = {
  dropboxToken:
    "rh1qXmGCjNAAAAAAAAAAB9Eeyrn-0tg7cQmmfYPxFUjwUEOSPbQnNUT65Eu4xog0",
  busy: false,
  errorMessage: ""
};

export default function settingsReducer(state = initialState, action) {
  switch (action.type) {
    case SAVE_SETTINGS: {
      if (action.dropboxToken) {
        const dropboxToken = action.dropboxToken;
        return {
          ...state,
          dropboxToken
        };
      }
      return state;
    }
    case SETTINGS_BUSY: {
      const busy = action.busy;
      return {
        ...state,
        busy
      };
    }
    case SETTINGS_ERROR: {
      const errorMessage = action.errorMessage;
      return {
        ...state,
        errorMessage
      };
    }
    case SETTINGS_RESET_ERROR: {
      const errorMessage = "";
      return {
        ...state,
        errorMessage
      };
    }
    default:
      return state;
  }
}
