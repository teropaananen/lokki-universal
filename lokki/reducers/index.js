import { combineReducers } from "redux";
import cottages from "./cottages";
import navRecuder from "../navigation/navReducer";
import booking from "./booking";
import date from "./date";
import settings from "./settings";

import { LOAD_STORAGE_TO_REDUX_STATE, DROPBOX_TO_REDUX } from "../constants";

const reducers = combineReducers({
  nav: navRecuder,
  cottages,
  booking,
  date,
  settings
});

// We want that LOAD_STORAGE_TO_REDUX_STATE updates whole reducer state
const tunedReducers = (state, action) => {
  if (action.type === LOAD_STORAGE_TO_REDUX_STATE) {
    return { ...action.data };
  } else if (action.type === DROPBOX_TO_REDUX) {
    return { ...action.data };
  }
  return reducers(state, action);
};

export default tunedReducers;
