import { SET_DATE } from "../constants";
import { normalizeDate } from "../helpers";

const initialState = {
  activeDate: dateToString(normalizeDate(new Date()))
};

export default function dateReducer(state = initialState, action) {
  switch (action.type) {
    case SET_DATE: {
      return {
        ...state,
        activeDate: dateToString(action.date)
      };
    }
    default:
      return state;
  }
}

export function dateToString(date) {
  if (typeof date === 'string') {
    return date;
  } else {
    return date.toISOString();
  }
}

export function dateStringToDate(dateString) {
  if (typeof dateString === 'string') {
    return new Date(dateString);
  } else {
    return dateString;
  }
}